import setuptools

setuptools.setup(
    name="builderbot",
    version="0.0.1",
    author="Nathan Rose",
    description="poly repo buld tool",
    packages=setuptools.find_packages(),
    tests_require=["pytest"],
)
