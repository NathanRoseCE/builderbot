import pytest
import sys
import os

if __name__ == '__main__':
    root_path = os.path.dirname(os.path.dirname(__file__))
    sys.path.append(root_path)
    sys.exit(pytest.main())
