"""Tests for dependency fetcher"""
import pytest
from unittest import mock
from unittest.mock import Mock, call

from builderbot_backend.dependency_fetcher import Fetcher

def test_load_repo_happy_path(mocker):
    mock_run = mocker.patch("subprocess.run")
    mock_run.return_value = Mock(returncode=0)
    fetcher = Fetcher("/opt/builderbot/cache")
    fetcher._load_repo("my_repo", "my_branch", "path")
    assert mock_run.called_once_with(
        "git --depth 1 --branch my_branch clone my_repo path"
    )

def test_load_repo_command_failed(mocker):
    mock_run = mocker.patch("subprocess.run")
    mock_run.return_value = Mock(returncode=1)
    fetcher = Fetcher("/opt/builderbot/cache")
    with pytest.raises(ValueError):
        fetcher._load_repo("my_repo", "my_branch", "path")

def test_validate_valid_configuration(mocker):
    fetcher = Fetcher("/opt/builderbot/cache")
    valid_configuration = {
        "project_name": "test_linking",
        "build_tool": "python_library",
        "python_requirements_file": "requirements.txt",
        "srcs": [
	    "setup.py",
	    "requirements.txt",
    	    "linking/*.py"
        ],
        "tests": [
	    "pytest.ini",
	    "test/*.py"
        ],
        "test_module": "pytest",
        "deps": [{	    
	    "repo_url": "git@gitlab.com:NathanRoseCE/roseauth_backend.git",
	    "commit": "master"
        }]
    }
    fetcher._validate_configuration(valid_configuration)

def test_validate_configuration_missing_project_name(mocker):
    fetcher = Fetcher("/opt/builderbot/cache")
    valid_configuration = {
        "build_tool": "python_library",
        "python_requirements_file": "requirements.txt",
        "srcs": [
	    "setup.py",
	    "requirements.txt",
    	    "linking/*.py"
        ],
        "tests": [
	    "pytest.ini",
	    "test/*.py"
        ],
        "test_module": "pytest",
        "deps": [{	    
	    "repo_url": "git@gitlab.com:NathanRoseCE/roseauth_backend.git",
	    "commit": "master"
        }]
    }
    with pytest.raises(ValueError):
        fetcher._validate_configuration(valid_configuration)

def test_validate_configuration_missing_build_tool(mocker):
    fetcher = Fetcher("/opt/builderbot/cache")
    valid_configuration = {
        "project_name": "test_linking",
        "python_requirements_file": "requirements.txt",
        "srcs": [
	    "setup.py",
	    "requirements.txt",
    	    "linking/*.py"
        ],
        "tests": [
	    "pytest.ini",
	    "test/*.py"
        ],
        "test_module": "pytest",
        "deps": [{	    
	    "repo_url": "git@gitlab.com:NathanRoseCE/roseauth_backend.git",
	    "commit": "master"
        }]
    }
    with pytest.raises(ValueError):
        fetcher._validate_configuration(valid_configuration)

def test_validate_configuration_missing_srcs(mocker):
    fetcher = Fetcher("/opt/builderbot/cache")
    valid_configuration = {
        "project_name": "test_linking",
        "build_tool": "python_library",
        "python_requirements_file": "requirements.txt",
        "tests": [
	    "pytest.ini",
	    "test/*.py"
        ],
        "test_module": "pytest",
        "deps": [{	    
	    "repo_url": "git@gitlab.com:NathanRoseCE/roseauth_backend.git",
	    "commit": "master"
        }]
    }
    with pytest.raises(ValueError):
        fetcher._validate_configuration(valid_configuration)

def test_validate_configuration_missing_deps(mocker):
    fetcher = Fetcher("/opt/builderbot/cache")
    valid_configuration = {
        "project_name": "test_linking",
        "build_tool": "python_library",
        "python_requirements_file": "requirements.txt",
        "srcs": [
	    "setup.py",
	    "requirements.txt",
    	    "linking/*.py"
        ],
        "tests": [
	    "pytest.ini",
	    "test/*.py"
        ],
        "test_module": "pytest"
    }
    with pytest.raises(ValueError):
        fetcher._validate_configuration(valid_configuration)

def test_validate_configuration_missing_tests(mocker):
    fetcher = Fetcher("/opt/builderbot/cache")
    valid_configuration = {
        "project_name": "test_linking",
        "build_tool": "python_library",
        "python_requirements_file": "requirements.txt",
        "srcs": [
	    "setup.py",
	    "requirements.txt",
    	    "linking/*.py"
        ],
        "test_module": "pytest",
        "deps": [{	    
	    "repo_url": "git@gitlab.com:NathanRoseCE/roseauth_backend.git",
	    "commit": "master"
        }]
    }
    with pytest.raises(ValueError):
        fetcher._validate_configuration(valid_configuration)

def test_get_build_filtes(mocker):
    glob_mock = mocker.patch("glob.glob")
    glob_mock.side_effect = [
        ["prepend/path/one", "prepend/path/two"],
        ["prepend/path/three", "prepend/path/four"],
        ["prepend/path/five", "prepend/path/six"]
    ]
    fetcher = Fetcher("/opt/builderbot/cache")
    config = {
        "srcs": [
            "src_one",
            "src_two"
        ],
        "tests": [
            "test_one"
        ]
    }
    assert fetcher._get_build_files(config, "prepend/path") == [
        "prepend/path/BuilderBot",
        "prepend/path/one",
        "prepend/path/two",
        "prepend/path/three",
        "prepend/path/four",
        "prepend/path/five",
        "prepend/path/six"
    ]
    assert glob_mock.call_count == 3
    glob_mock.assert_has_calls([
        call("prepend/path/src_two"),
        call("prepend/path/src_one"),
        call("prepend/path/test_one")
    ], any_order=True)
        


def test_load_valid_build_configuration(mocker):
    mocker.patch("os.path.exists", return_value=True)
    fetcher = Fetcher("/opt/builderbot/cache")
    with mock.patch("builtins.open", mock.mock_open(
            read_data="""{
    "project_name": "test_linking",
    "build_tool": "python_library",
    "python_requirements_file": "requirements.txt",
    "srcs": [
	"setup.py",
	"requirements.txt",
    	"linking/*.py"
    ],
    "tests": [
	"pytest.ini",
	"test/*.py"
    ],
    "test_module": "pytest",
    "deps": [{	    
	"repo_url": "git@gitlab.com:NathanRoseCE/roseauth_backend.git",
	"commit": "master"
    }]
}""")):
        assert fetcher._load_build_configuration("this_is_mocked_who_cares") == {
            "project_name": "test_linking",
            "build_tool": "python_library",
            "python_requirements_file": "requirements.txt",
            "srcs": [
	        "setup.py",
	        "requirements.txt",
    	        "linking/*.py"
            ],
            "tests": [
	        "pytest.ini",
	        "test/*.py"
            ],
            "test_module": "pytest",
            "deps": [{	    
	        "repo_url": "git@gitlab.com:NathanRoseCE/roseauth_backend.git",
	        "commit": "master"
            }]
        }

def test_load_build_configuration_doesnt_exist(mocker):
    mocker.patch("os.path.exists", return_value=False)
    fetcher = Fetcher("/opt/builderbot/cache")
    with pytest.raises(ValueError):
        fetcher._load_build_configuration("meh")

def test_load_build_configuration_invalid_config(mocker):
    mocker.patch("os.path.exists", return_value=True)
    fetcher = Fetcher("/opt/builderbot/cache")
    with pytest.raises(ValueError):
            with mock.patch("builtins.open", mock.mock_open(
                    read_data="""{
    "project_name": "test_linking",
    "build_tool": "python_library",
    "python_requirements_file": "requirements.txt",
    "tests": [
	"pytest.ini",
	"test/*.py"
    ],
    "test_module": "pytest",
    "deps": [{	    
	"repo_url": "git@gitlab.com:NathanRoseCE/roseauth_backend.git",
	"commit": "master"
    }]
}""")):
                fetcher._load_build_configuration("meh")
