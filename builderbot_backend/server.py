import asyncio
import json
from typing import Dict, Any
import traceback
import websockets

from builderbot_backend.executor import Executor
from builderbot_backend.dependency_fetcher import Fetcher
from builderbot_backend.python_builder import LibraryBuilder
from builderbot_backend.docker_image import DockerImageBuilder
from builderbot_backend.utils import Reporter


executor = Executor(
    "/opt/builderbot/cache/{project_name}/{cache_hash}/",
    Fetcher("/opt/builderbot/cache/{project_name}/{cache_hash}/"),
    [LibraryBuilder(), DockerImageBuilder()]
)


async def build_request(request, event_reporter):
    repo_url = request["repo_url"]
    commit = request["commit"]
    if repo_url is None or commit is None:
        return "Must specify repo_url and commit"
    return await executor.build_request(request, event_reporter)

async def clean(request, event_reporter):
    await executor.clean(event_reporter)
    return {}

async def request(websocket):
    while True:
        try:
            request_str = await websocket.recv()
            request = json.loads(request_str)
            event_reporter = Reporter(websocket)
            print(f"Recieved request: {request}")
            if request["request"] in ["release", "deploy"]:
                try:
                    await build_request(request, event_reporter)
                except Exception as error:
                    print(repr(error))
                    traceback.print_exc()
                    await event_reporter.close()
                    await websocket.send(json.dumps({
                        "type": "status",
                        "status": "Failure"
                    }))
                    continue
                await event_reporter.close()
                await websocket.send(json.dumps({
                    "type": "status",
                    "status": "Success"
                }))
            elif request["request"] == "clean":
                await clean(request, event_reporter)
                await event_reporter.close()
                await websocket.send(json.dumps({
                    "type": "status",
                    "status": "Success"
                }))
            else:
                await websocket.send(f"Unkown request type '{request}'")
        except Exception as error:
            traceback.print_exc()
            print(f"Internal server error: {repr(error)}")
            raise

async def main():
    async with websockets.serve(request, "localhost", 54294):
        await asyncio.Future()

if __name__ == '__main__':
    asyncio.run(main())
