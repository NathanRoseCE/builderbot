from functools import cache
import logging
import glob
import os
import pathlib
from typing import Dict, List, Any
from builderbot_backend.builder import Builder, BuildTask
from builderbot_backend.utils import run_bash_command, get_aws_credentials

class DockerImageBuilder(Builder):
    """
    Python library builder
    """
    BUILD_TOOL_NAME="docker_image"

    def __init__(self) -> None:
        self._logger = logging.getLogger()
        self._logger.setLevel(logging.DEBUG)

    async def setup(self, base_path: str) -> None:
        pass

    async def install_dependency(self, base_path: str, dependency_type: str, dependency_files: List[str])->None:
        pass

    async def build(self, base_path: str, build_config: Dict[str, str]) -> List[str]:
        assert build_config["build_tool"] == self.BUILD_TOOL_NAME
        
        self._logger.info(f"building {base_path}")
        await run_bash_command(
            "mkdir -p bin/ && cp src/Dockerfile .; docker buildx build --platform {PLATFORMS} -t {TAG} -o type=tar,dest=bin/image.tar .".format(
                PLATFORMS=",".join(build_config["platforms"]),
                TAG=build_config["tag"]
            ),
            output_file_path=os.path.join(base_path, "logs", "build.log"),
            cwd=os.path.join(base_path)
        )
        return [
            os.path.join(base_path, "bin", python_lib)
            for python_lib in os.listdir(os.path.join(base_path, "bin"))
        ]

    async def test(self, base_path: str, build_config: Dict[str, str]) -> None:
        """... not sure what tests to run"""
        pass

    async def deploy(self, base_path: str, build_config: Dict[str, Any]) -> None:
        """Deploy the docker image."""
        if "deploy" not in build_config:
            print("nothing to deploy")
            return
        if build_config["deploy"]["provider"] != "aws":
            raise ValueError(f"{build_config['deploy']['provider']} provider is not supported")
        builder_creds = get_aws_credentials(
            role_arn="arn:aws:iam::278275460649:role/build_system",
            session_name="docker_build_"+build_config["project_name"]
        )
        push_creds = get_aws_credentials(
            role_arn=build_config["deploy"]["role"],
            session_name="docker_build_"+build_config["project_name"],
            with_credentials=builder_creds
        )
        await run_bash_command(
            "aws sts get-caller-identity; aws ecr get-login-password --region us-east-1 | docker login --username AWS --password-stdin {ENDPOINT}".format(
                ENDPOINT=build_config["deploy"]["endpoint"],
            ),
            output_file_path=os.path.join(base_path, "logs", "docker_login.log"),
            env_vars={
                "AWS_ACCESS_KEY_ID": push_creds["AccessKeyId"],
                "AWS_SECRET_ACCESS_KEY": push_creds["SecretAccessKey"],
                "AWS_SESSION_TOKEN": push_creds["SessionToken"]
            },
            cwd=os.path.join(base_path)
        )
        
        print(f"loading image {glob.glob(os.path.join(base_path, 'bin', '*.tar'))}")
        for image in glob.glob(os.path.join(base_path, "bin","*.tar")):
            await run_bash_command(
                "cat {IMAGE_TAR_PATH} | docker import - {TAG}".format(
                    IMAGE_TAR_PATH=image,
                    TAG=build_config["tag"]
                ),
                output_file_path=os.path.join(base_path, "logs", "docker_load.log"),
                cwd=os.path.join(base_path)
        )
        await run_bash_command(
            "docker tag {TAG} {DEPLOY_TAG} && docker push {DEPLOY_TAG}".format(
                PLATFORMS=",".join(build_config["platforms"]),
                TAG=build_config["tag"],
                DEPLOY_TAG=f'{build_config["deploy"]["endpoint"]}/{build_config["tag"]}'
            ),
            output_file_path=os.path.join(base_path, "logs", "docker_push.log"),
            cwd=os.path.join(base_path)
        )
        
