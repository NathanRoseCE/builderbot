import logging
import pathlib
import os
import json
import subprocess
import shutil
from typing import List, Dict, Any
import traceback

from builderbot_backend.dependency_fetcher import Fetcher
from builderbot_backend.builder import Builder, BuildTask, BuildTaskManager
from builderbot_backend.utils import run_bash_command, Reporter

class Executor:
    def __init__(self, cache_path: str, fetcher: Fetcher, builders: List[Builder]) -> None:
        self._fetcher = fetcher
        self._builders = builders
        self._logger = logging.getLogger(__name__)
        self._cache_path = cache_path.rstrip("/")
        if not os.path.exists(os.path.dirname(os.path.dirname(self._cache_path))):
            os.makedirs(os.path.dirname(os.path.dirname(self._cache_path)))

    async def build_request(self, request: Dict[str, Any], event_handler: Reporter) -> str:
        """
        submits a build request
        """
        repo_url = request["repo_url"]
        commit = request["commit"]
        event_handler.event({"Event": "Fetching Dependencies"})
        build_requests = self._fetcher.recursive_fetch(repo_url, commit)
        event_handler.event({"Event": "All dependencies fetched"})

        build_manager = BuildTaskManager(reporter=event_handler, request_type=request["request"])
        build_manager.process_build_requests(build_requests)

        task = self._build_worker(event_handler, build_manager)
        await task
        return "Build Complete"

    async def _build_worker(self, reporter: Reporter, build_manager: BuildTaskManager) -> None:
        while True:
            build_task = build_manager.get_build_request()
            if build_task:
                try:
                    await self._build(build_task, reporter, build_manager)
                    build_manager.build_success(build_task.project_name)
                except Exception:
                    traceback.print_exc()
                    build_manager.build_failure(build_task.project_name)
                continue
            test_task = build_manager.get_test_request()
            if test_task:
                try:
                    await self._test(test_task, reporter)
                    build_manager.test_success(test_task.project_name)
                except Exception:
                    traceback.print_exc()
                    build_manager.test_failure(test_task.project_name)
                continue
            deploy_task = build_manager.get_deploy_request()
            if deploy_task:
                try:
                    await self._deploy(deploy_task, reporter)
                    build_manager.deploy_success(deploy_task.project_name)
                except Exception:
                    traceback.print_exc()
                    build_manager.deploy_failure(deploy_task.project_name)
                continue
            return

    async def setup(self, build_task: BuildTask, reporter: Reporter, build_manager: BuildTaskManager) -> None:
        cache_path = self._cache_path.format(
            project_name=build_task.project_name,
            cache_hash=build_task.project_hash
        )
        if self.is_fetched(cache_path):
            reporter.event({"Event": f"Using cached fetch of {build_task.project_name}"})
            return
        self._setup_cache(cache_path)
        builder = self.get_builder(build_task.config["build_tool"])
        reporter.event({"Event": f"Setup {build_task.project_name}"})
        await builder.setup(cache_path)
        reporter.event({"Event": f"Setup {build_task.project_name} complete"})
        for dependency_name in build_task.deps:
            print(f"installing dep: {dependency_name}")
            dependency = build_manager.get_task(dependency_name)
            dep_cache_path = self._cache_path.format(
                project_name=dependency.project_name,
                cache_hash=dependency.project_hash
            )
            await builder.install_dependency(
                base_path=cache_path,
                dependency_type=dependency.config["build_tool"],
                dependency_files=self._get_builderbot_output_files(dep_cache_path)
            )
        self.is_fetched(cache_path)
                
    async def _build(self, build_task: BuildTask, reporter: Reporter, build_manager: BuildTaskManager) -> None:
        """Performs the build step for a build_request."""
        await self.setup(build_task, reporter, build_manager)
        cache_path = self._cache_path.format(
            project_name=build_task.project_name,
            cache_hash=build_task.project_hash
        )
        if self.is_built(cache_path):
            reporter.event({"Event": f"Using cached build of {build_task.project_name}"})
            return
        builder = self.get_builder(build_task.config["build_tool"])
        reporter.event({"Event": f"Building {build_task.project_name}"})
        try:
            build_output = await builder.build(cache_path, build_task.config)
        except Exception:
            build_output = ""
            with open(os.path.join(cache_path, "log", "build.log")) as build_log:
                build_output = build_log.read()
            reporter.step_output(build_task.project_name, build_output)
            reporter.event({"Event": f"Build {build_task.project_name} Failed"})
            raise
        reporter.event({"Event": f"Build {build_task.project_name} Success"})
        self._post_build(cache_path, [
            os.path.join(cache_path, "bin", file_name)
            for file_name in os.listdir(os.path.join(cache_path, "bin"))
        ])

    async def _test(self, build_task: BuildTask, reporter: Reporter) -> None:
        """actually performs the build."""
        cache_path = self._cache_path.format(
            project_name=build_task.project_name,
            cache_hash=build_task.project_hash
        )
        if self.is_tested(cache_path):
            reporter.event({"Event": f"cached of test {build_task.project_name}"})
            return
        builder = self.get_builder(build_task.config["build_tool"])
        test_output = ""
        try:
            reporter.event({"Event": f"Testing {build_task.project_name}"})
            await builder.test(cache_path, build_task.config)
            reporter.event({"Event": f"Testing {build_task.project_name} Success"})
        except Exception:
            test_output = ""
            with open(os.path.join(cache_path, "logs", "test.log")) as test_log:
                test_output = test_log.read()
            reporter.step_output(build_task.project_name, test_output)
            reporter.event({"Event": f"Build {build_task.project_name} Failed"})
            raise
        reporter.event({"Event": f"Cleanup {build_task.project_name}"})
        self._post_test(cache_path)

    async def _deploy(self, build_task: BuildTask, reporter: Reporter) -> None:
        """actually performs the build."""
        cache_path = self._cache_path.format(
            project_name=build_task.project_name,
            cache_hash=build_task.project_hash
        )
        if self.is_deployed(cache_path):
            reporter.event({"Event": f"cached deploy of {build_task.project_name}"})
            return
        builder = self.get_builder(build_task.config["build_tool"])
        test_output = ""
        try:
            reporter.event({"Event": f"Deploying {build_task.project_name}"})
            await builder.deploy(cache_path, build_task.config)
            reporter.event({"Event": f"Deploy {build_task.project_name} Success"})
        except Exception:
            deploy_output = ""
            with open(os.path.join(cache_path, "logs", "deploy.log")) as deploy_log:
                deploy_output = deploy_log.read()
            reporter.step_output(build_task.project_name, deploy_output)
            reporter.event({"Event": f"Build {build_task.project_name} Failed"})
            raise
        reporter.event({"Event": f"Cleanup {build_task.project_name}"})
        self._post_deploy(cache_path)

    def get_builder(self, build_tool: str) -> Builder:
        for builder in self._builders:
            if build_tool == builder.BUILD_TOOL_NAME:
                return builder
        raise ValueError(f"Unkown builder {build_tool}")


    def _setup_cache(self, cache_path: str) -> None:
        if not os.path.exists(os.path.join(cache_path, "deps")):
            os.mkdir(os.path.join(cache_path, "deps"))
        if not os.path.exists(os.path.join(cache_path, "logs")):
            os.mkdir(os.path.join(cache_path, "logs"))

    def _copy_dependency(self, dependent_path: str, dependency_path: str) -> None:
        subprocess.run(
            "cp -rf {} {}".format(
                os.path.join(dependent_path, "bin/."),
                os.path.join(dependency_path, "deps/.")
            ).split(" ")
        )

    def is_built(self, base_dir: str) -> bool:
        """Checks whether we built it already."""
        return os.path.exists(os.path.join(base_dir, "meta", "built"))

    def _post_build(self, base_dir: str, output_files) -> None:
        self._save_builderbot_output(base_dir, output_files)
        if not os.path.exists(os.path.join(base_dir, "meta")):
            os.makedirs(os.path.join(base_dir, "meta"))
        pathlib.Path(os.path.join(base_dir, "meta", "built")).touch()

    def is_tested(self, base_dir: str) -> bool:
        """Checks whether we built it already."""
        return os.path.exists(os.path.join(base_dir, "meta", "tested"))

    def _post_test(self, base_dir: str) -> None:
        if not os.path.exists(os.path.join(base_dir, "meta")):
            os.makedirs(os.path.join(base_dir, "meta"))
        pathlib.Path(os.path.join(base_dir, "meta", "tested")).touch()

    def is_fetched(self, base_dir: str) -> bool:
        """Checks whether we built it already."""
        return os.path.exists(os.path.join(base_dir, "meta", "fetched"))

    def _post_fetch(self, base_dir: str) -> None:
        if not os.path.exists(os.path.join(base_dir, "meta")):
            os.makedirs(os.path.join(base_dir, "meta"))
        pathlib.Path(os.path.join(base_dir, "meta", "fetched")).touch()
        
    def is_deployed(self, base_dir: str) -> bool:
        """Checks whether we built it already."""
        return os.path.exists(os.path.join(base_dir, "meta", "deployed"))

    def _post_deploy(self, base_dir: str) -> None:
        if not os.path.exists(os.path.join(base_dir, "meta")):
            os.makedirs(os.path.join(base_dir, "meta"))
        pathlib.Path(os.path.join(base_dir, "meta", "deployed")).touch()

    def _save_builderbot_output(self, base_path: str, output_files: List[str]) -> None:
        if not os.path.exists(os.path.join(base_path, "builderbot_out")):
            os.makedirs(os.path.join(base_path, "builderbot_out"))
        for output_file in output_files:
            shutil.copy(
                output_file,
                os.path.join(base_path, "builderbot_out", os.path.basename(output_file))
            )
        
    def _get_builderbot_output_files(self, base_path: str) -> List[str]:
        return [
            os.path.join(base_path, "builderbot_out", output_file)
            for output_file in os.listdir(os.path.join(base_path, "builderbot_out"))
        ]

    async def clean(self, event_reporter: Reporter) -> None:
        await run_bash_command(
            "rm -rf *",
            cwd=f"{os.path.dirname(os.path.dirname(self._cache_path))}"
        )
