import logging
import os
import pathlib
from typing import Dict, List, Any
from builderbot_backend.builder import Builder, BuildTask
from builderbot_backend.utils import run_bash_command

class LibraryBuilder(Builder):
    """
    Python library builder
    """
    BUILD_TOOL_NAME="python_library"

    def __init__(self) -> None:
        self._logger = logging.getLogger()
        self._logger.setLevel(logging.DEBUG)

    async def setup(self, base_path: str) -> None:
        await self._setup_python_venv(base_path)
        await self._install_pip_requirements(base_path)

    async def install_dependency(self, base_path: str, dependency_type: str, dependency_files: List[str]) -> None:
        if dependency_type == "python_library":
            await self._install_python_deps(base_path, dependency_files)
        else:
            raise ValueError(f"Unknown dependency type: {dependency_type}")

    async def build(self, base_path: str, build_config: Dict[str, str]) -> List[str]:
        assert build_config["build_tool"] == self.BUILD_TOOL_NAME
        
        self._logger.info(f"building {base_path}")
        if not os.path.exists(os.path.join(base_path, "bin")):
            os.mkdir(os.path.join(base_path, "bin"))
        await run_bash_command(
            ". ../venv/bin/activate && python setup.py sdist && cp dist/* ../bin && rm -rf dist/".format(
                os.path.join(base_path, "bin"),
            ),
            output_file_path=os.path.join(base_path, "logs", "build.log"),
            cwd=os.path.join(base_path, "src")
        )
        return [
            os.path.join(base_path, "bin", python_lib)
            for python_lib in os.listdir(os.path.join(base_path, "bin"))
        ]

    async def test(self, base_path: str, build_config: Dict[str, str]) -> None:
        """
        run python tests
        """
        await run_bash_command(
            ". ../venv/bin/activate && python -m {}".format(
                build_config.get("test_module", "pytest")
            ),
            output_file_path=os.path.join(base_path, "logs", "test.log"),
            cwd=os.path.join(base_path, "src")
        )
                    
    async def _install_python_deps(self, base_path: str, dependency_files: List[str]) -> None:
        """
        Install the dependencies in a requirements.txt file
        """
        self._logger.info(f"Installing Builderbot deps for {base_path}: {dependency_files}")
        deps_python = [
            dependency_file for dependency_file in dependency_files
            if ".tar.gz" in dependency_file
        ]
        for dep_python in deps_python:
            await run_bash_command(
                ". ../venv/bin/activate && python -m pip install --upgrade {}".format(
                    dep_python
                ),
                cwd=os.path.join(base_path, "deps")
            )
        self._logger.info(f"Complete installing builderbot deps for: {base_path}")

    async def _setup_python_venv(self, base_path: str) -> None:
        """
        setup the python build environment
        """
        self._logger.info(f"Setting up venv for {base_path}")
        await run_bash_command(
            "python -m venv venv/",
            cwd=base_path
        )
        self._logger.info(f"Venv setup for {base_path}")
    
    async def _install_pip_requirements(self, base_path: str) -> None:
        self._logger.info(f"Installing Pip requirements file for {base_path}")
        requirements_file = os.path.join(base_path, "src", "requirements.txt")
        if not os.path.exists(requirements_file):
            self._logger.info("python_requirements_file not specified.")
            return
        await run_bash_command(
            ". venv/bin/activate && python -m pip install -r {}".format(
                requirements_file
            ),
            cwd=os.path.join(base_path)
        )
        self._logger.info(f"Complete installing requirements.txt deps for: {base_path}")

    

    async def deploy(self, base_path: str, config: Dict[str, Any]) -> None:
        """No deploy logic implimented for python libraries."""
        
