from __future__ import annotations
"""
A build task is a build step that can be completed
"""
from typing import Dict, List

from enum import Enum


class BuildState(Enum):
    KNOWN = 0
    FETCHING = 1
    FETCHED = 2
    FETCED = 3
    FETCH_FAIL = 4
    DEP_BUILD_FAILURE = 5
    BUILDING = 6
    BUILT = 7
    BUILD_FAILURE = 8
    TESTING = 9
    TEST_FAILURE = 10
    TESTED = 11
    DEP_TEST_FAILURE= 12
    DEPLOYING = 13
    DEPLOY_FAILED = 14
    SUCCESS = 15


class BuildTask:
    """A step to build some software."""

    def __init__(self,
                 project_name: str,
                 project_hash: str,
                 repo_url: str,
                 commit: str,
                 config: Dict[str,str],
                 deps: List[str]) -> None:
        self.project_name = project_name
        self.project_hash = project_hash
        self.repo_url = repo_url
        self.commit = commit
        self.config = config
        self.deps = deps
        self.state = BuildState.KNOWN

