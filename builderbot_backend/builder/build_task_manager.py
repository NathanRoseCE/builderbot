"""
This is for a build task manager
"""
from typing import List, Dict, Optional
from builderbot_backend.builder.build_task import BuildTask, BuildState
from builderbot_backend.builder.exceptions import DiamondDependency
from builderbot_backend.utils import Reporter

class BuildTaskManager:
    def __init__(self, reporter: Reporter, request_type: str) -> None:
        self._reporter = reporter
        self._all_tasks: Dict[str, BuildTask] = {}
        self._build_queue: List[str]  = []
        self._test_queue: List[str] = []
        self._deploy_queue: List[str] = []
        self._inverted_deps: Dict[str, List[str]] = {}
        self._request_type = request_type

    @property
    def reporter(self) -> Reporter:
        return self._reporter

    def process_build_requests(self, build_requests: List[BuildTask]) -> None:
        self._reporter.event({"msg": "Processing fetched dependenency tree"})
        sanitized_build_reqs = self._validate_request(build_requests)
        self._all_tasks = {
            request.project_name: request
            for request in sanitized_build_reqs
        }
        self._register_dependency_tree()
        self._bootstrap_queues()
        self._reporter.event({"msg": "Processed dependency tree"})
        

    def _register_dependency_tree(self) -> None:
        for project_name, build_request in self._all_tasks.items():
            for dep in build_request.deps:
                print(f"dep registered: {project_name} -> {dep}")
                self._register_dep(project_name, dep)

    def _bootstrap_queues(self):
        for project_name, request in self._all_tasks.items():
            if not request.deps:
                self._build_queue.append(project_name)

    def get_build_request(self) -> Optional[BuildTask]:
        """Gets a task to perform a build on, returns None if cannot."""
        if not self._build_queue:
            return None
        task_to_build = self._all_tasks[self._build_queue.pop()]
        task_to_build.state = BuildState.BUILDING
        self._update_status(task_to_build)
        return task_to_build

    def build_success(self, project_name: str) -> None:
        """Call when a build request was a success."""
        self._all_tasks[project_name].state = BuildState.BUILT
        self._update_status(self._all_tasks[project_name])
        if self._request_type not in ["test", "release", "deploy"]:
            return
        self._test_queue.append(project_name)
        for dep in self._get_inverted_deps_of(project_name):
            self._put_on_build_queue_if_no_blocking_deps(dep)

    def build_failure(self, project_name: str) -> None:
        """Call when a build request was a failure."""
        self._all_tasks[project_name].state = BuildState.BUILD_FAILURE
        self._update_status(self._all_tasks[project_name])
        for inverted_dep in self._get_inverted_deps_of(project_name):
            self._build_dep_failure(inverted_dep)

    def get_test_request(self) -> Optional[BuildTask]:
        """Gets a task to perform a test on, returns None if it cannot."""
        if not self._test_queue:
            return None
        task_to_test = self._all_tasks[self._test_queue.pop()]
        task_to_test.state = BuildState.TESTING
        self._update_status(task_to_test)
        return task_to_test
        
    def test_success(self, project_name: str) -> None:
        """Call when a build request was a success."""
        self._all_tasks[project_name].state = BuildState.TESTED
        self._update_status(self._all_tasks[project_name])
        if self._request_type not in ["deploy"]:
            return
        self._put_on_deploy_queue_if_no_blocking_deps(project_name)

    def test_failure(self, project_name: str) -> None:
        """Call when a build request was a failure."""
        self._all_tasks[project_name].state = BuildState.TEST_FAILURE
        self._update_status(self._all_tasks[project_name])
        for dep in self._get_inverted_deps_of(project_name):
            self._dep_test_failure(dep)

    def get_deploy_request(self) -> Optional[BuildTask]:
        """Gets a task to perform a build on, returns None if cannot."""
        if not self._deploy_queue:
            return None
        task_to_deploy = self._all_tasks[self._deploy_queue.pop()]
        task_to_deploy.state = BuildState.DEPLOYING
        self._update_status(task_to_deploy)
        return task_to_deploy

    def deploy_success(self, project_name: str) -> None:
        """Call when a deploy request was a success."""
        self._all_tasks[project_name].state = BuildState.SUCCESS
        self._update_status(self._all_tasks[project_name])
        for dep in self._get_inverted_deps_of(project_name):
            self._put_on_deploy_queue_if_no_blocking_deps(dep)

    def deploy_failure(self, project_name: str) -> None:
        """Call when a deploy request was a failure."""
        self._all_tasks[project_name].state = BuildState.DEPLOY_FAILED
        self._update_status(self._all_tasks[project_name])
        
    def _put_on_build_queue_if_no_blocking_deps(self, project_name: str) -> None:
        for dep in self._all_tasks[project_name].deps:
            if self._all_tasks[dep].state not in [
                    BuildState.BUILT,
                    BuildState.TESTING,
                    BuildState.TEST_FAILURE,
                    BuildState.TESTED,
                    BuildState.DEPLOYING,
                    BuildState.DEPLOY_FAILED,
                    BuildState.SUCCESS
            ]:
                return
        self._build_queue.append(project_name)
        
    def _put_on_deploy_queue_if_no_blocking_deps(self, project_name: str) -> None:
        for dep in self._all_tasks[project_name].deps:
            if self._all_tasks[dep].state not in [
                    BuildState.SUCCESS
            ]:
                return
        self._deploy_queue.append(project_name)

    def _build_dep_failure(self, project_name: str) -> None:
        self._all_tasks[project_name].state = BuildState.DEP_BUILD_FAILURE
        self._update_status(self._all_tasks[project_name])
        for inverted_dep in self._get_inverted_deps_of(project_name):
            self._build_dep_failure(inverted_dep)

    def _dep_test_failure(self, project_name: str) -> None:
        if self._all_tasks[project_name].state != BuildState.TESTED:
            # not waiting to be deployed
            return
        self._all_tasks[project_name].state = BuildState.DEP_TEST_FAILURE
        self._update_status(self._all_tasks[project_name])
        for inverted_dep in self._get_inverted_deps_of(project_name):
            self._dep_test_failure(inverted_dep)
                

    def _validate_request(self, build_requests: List[BuildTask]) -> List[BuildTask]:
        seen_reqs: Dict[str, BuildTask] = {}
        for build_req in build_requests:
            if build_req.project_name in seen_reqs:
                if seen_reqs[build_req.project_name].project_hash != build_req.project_hash:
                    raise DiamondDependency(build_req.project_name)
            seen_reqs[build_req.project_name] = build_req
        return list(seen_reqs.values())

    def _update_status(self, build_task: BuildTask) -> None:
        self._reporter.build_status({
            "project": build_task.project_name,
            "hash": build_task.project_hash,
            "status": build_task.state.name
        })

    def _register_dep(self, project: str, dependency: str) -> None:
        if dependency not in self._inverted_deps:
            self._inverted_deps[dependency] = []
        self._inverted_deps[dependency].append(project)

    def _get_inverted_deps_of(self, project: str) -> List[str]:
        if project not in self._inverted_deps:
            return []
        return self._inverted_deps[project]

    def get_task(self, project_name: str) -> BuildTask:
        return self._all_tasks[project_name]
        
