from typing import List, Dict, Any
from abc import ABC, abstractmethod

class Builder(ABC):
    """
    Interface for a builder
    expects the code to be in {root_path}/src
    will output the build {root_path}/logs/build.log
    build shuold be done in {root_path}/bin folder
    """
    BUILD_TOOL_NAME="UNDEFINED"
    
    @staticmethod
    async def setup(self,
                    base_path: str) -> None:
        """A function called before anything else."""
        raise NotImplementedError()

    @abstractmethod
    async def install_dependency(self,
                           base_path: str,
                           dependency_type: str,
                           dependency_files: List[str]) -> None:
        """Install a BuilderBot dependency."""
        raise NotImplementedError()

    @abstractmethod
    async def build(self, base_path: str, build_config) -> List[str]:
        """
        Build the code, outputs the files that are built.

        This code can assume that all builderbot dependency's have been isntalled
        with install_dependency
        """
        raise NotImplementedError()

    @abstractmethod
    async def test(self, base_path: str, build_config) -> str:
        """
        Run the tests.

        Can assume build has been run
        """
        raise NotImplementedError()

    @abstractmethod
    async def deploy(self, base_path: str, build_config) -> str:
        """
        Deploy the code

        Can assume build has been run
        """
        raise NotImplementedError()
        


