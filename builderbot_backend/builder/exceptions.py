
class DiamondDependency(Exception):
    """An exception for a diamond dependency"""
    def __init__(self, project_name):
        super().__init(f"A diamond dependency detected with {project_name}")
