import glob
import hashlib
import json
import logging
import os
import shutil
import subprocess
import tempfile
from typing import Dict, List

from builderbot_backend.builder import BuildTask

class Fetcher:
    """
    Fetches the dependencies
    assumes credentials are setup
    """
    
    def __init__(self, base_path: str) -> None:
        self._logger = logging.getLogger(__name__)
        self._logger.setLevel(logging.DEBUG)
        self._base_path = base_path

    def recursive_fetch(self, repo_url: str, commit: str) -> List[BuildTask]:
        """Recursively fetch all dependencies"""
        self._logger.info(f"Fetching {repo_url}:{commit}")
        temp_path = tempfile.mkdtemp()
        self._load_repo(repo_url, commit, temp_path)
        build_configuration = self._load_build_configuration(temp_path)
        build_items = []
        direct_dep_names = []
        for dep in build_configuration["deps"]:
            new_build_items = self.recursive_fetch(dep["repo_url"], dep["commit"])
            direct_dep_names.append(new_build_items[0].project_name)
            build_items = build_items + new_build_items
        dep_hash = self._hash_dependency_hashes([
            build_item.project_hash for build_item in build_items
        ])
        my_hash = self._move_files(build_configuration, temp_path, dep_hash)
        return [
            BuildTask(
                project_name=build_configuration["project_name"],
                project_hash=my_hash,
                repo_url=repo_url,
                commit=commit,
                config=build_configuration,
                deps=direct_dep_names
            )
        ] + build_items
    
        
    def fetch(self, repo_url: str, commit: str, deps_hash: str) -> str:
        """
        Fetcher a builderbot repo.
        returns the hash of the build files,
        files will be stored in /opt/builderbot/cache/{project_name}/{hash}
        """
        temp_path = tempfile.mkdtemp()
        self._load_repo(repo_url, commit, temp_path)
        build_configuration = self._load_build_configuration(temp_path)
        return self._move_files(build_configuration, temp_path, deps_hash)

    def _load_repo(self, repo_url, commit: str, temp_path: str) -> None:
        """Load the requested repo into a temp directory."""
        self._logger.info("Fetching: {repo_url}")
        cmd = f"git clone --depth 1 --branch {commit} {repo_url} {temp_path}"
        result = subprocess.run(
            cmd.split(" "),
            capture_output=True
        )
        if result.returncode != 0:
            self._logger.error(f"An error occured running: {cmd}")
            self._logger.error(f"stdout: {result.stdout}")
            self._logger.error(f"stderr: {result.stderr}")
            raise ValueError()
        
    def _move_files(self, build_configuration: Dict[str, str], temp_path: str, deps_hash: str) -> str:
        """
        move the file and return the hash
        """
        build_files = self._get_build_files(build_configuration, temp_path)
        cache_hash = self._generate_build_hash(build_files, deps_hash)
        cache_path = self._base_path.format(
            project_name=build_configuration["project_name"],
            cache_hash=cache_hash
        )
        self._copy_build_files(temp_path, build_files, os.path.join(cache_path, "src"))
        return cache_hash

    def _load_build_configuration(self, repo_path: str) -> Dict[str,str]:
        """Loads and validates a configuration."""
        config_file_path = os.path.join(repo_path, "BuilderBot")
        if not os.path.exists(config_file_path):
            self._logger.error("Repo is not a builderbot repo!")
            raise ValueError()
        build_configuration = {}
        with open(config_file_path) as config_file:
            build_configuration = json.load(config_file)
        self._validate_configuration(build_configuration)
        return build_configuration

    def _validate_configuration(self, config: Dict[str,str]):
        """Validates configuration"""
        if not isinstance(config.get("project_name"), str):
            self._logger.error("missing project_name field")
            raise ValueError()
        if not isinstance(config.get("build_tool"), str):
            self._logger.error("missing build_tool field")
            raise ValueError()
        if not isinstance(config.get("srcs"), list):
            self._logger.error("missing srcs field")
            raise ValueError()
        if not isinstance(config.get("tests"), list):
            self._logger.error("missing tests field")
            raise ValueError()
        if not isinstance(config.get("deps"), list):
            self._logger.error("missing srcs field")
            raise ValueError()

    def _get_build_files(self, build_config: Dict[str,str], path: str) -> List[str]:
        """gets the build files."""
        files = [
            os.path.join(path, "BuilderBot")
        ]
        for src_file_glob in build_config["srcs"]:
            for globbed_file in glob.glob(os.path.join(path, src_file_glob)):
                files.append(globbed_file)
        for test_file_glob in build_config["tests"]:
            for globbed_file in glob.glob(os.path.join(path, test_file_glob)):
                files.append(globbed_file)
        return files

    def _generate_build_hash(self, build_files, deps_hash: str) -> str:
        """
        generates a hash from the build files
        """
        BUF_SIZE = 65536
        md5 = hashlib.md5()
        md5.update(bytes(deps_hash, encoding="utf-8"))
        for build_file in build_files:
            with open(build_file, 'rb') as f:
                while True:
                    data = f.read(BUF_SIZE)
                    if not data:
                        break
                    md5.update(data)
        return md5.hexdigest()

    def _copy_build_files(self, temp_path: str, build_files: List[str], cache_path: str) -> None:
        """
        copies the build files
        """
        for build_file in build_files:
            relative_path = build_file.split(os.path.join(temp_path) + "/")[1]
            final_path = os.path.join(cache_path, relative_path)
            if not os.path.exists(os.path.dirname(final_path)):
                os.makedirs(os.path.dirname(final_path))
            self._logger.info("Copying file from {} to {}", relative_path, final_path)
            shutil.copyfile(build_file, final_path)

    def _hash_dependency_hashes(self, dependency_hashes: List[str]):
        if not dependency_hashes:
            return ""
        md5 = hashlib.md5()
        for dep_hash in dependency_hashes:
            md5.update(bytes(dep_hash, encoding="utf-8"))
        return md5.hexdigest()
