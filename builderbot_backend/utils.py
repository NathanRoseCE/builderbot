import asyncio
from asyncio import subprocess
import boto3
import json
import logging
from typing import Dict, Any, Optional

LOGGER = logging.getLogger(__file__)

async def run_bash_command(command: str,
                           cwd: str,
                           env_vars: Dict[str,str]={},
                           output_file_path: Optional[str]=None,
                           ignore_nonzero_returncode=False) -> None:
    print(f"running {command} in {cwd}")
    env_vars_prefix_cmd = "".join(
        f"export {key}={value}; " for key, value in env_vars.items()
    )
    full_cmd = env_vars_prefix_cmd + command
    print(f"full_cmd: {full_cmd}")
    result = await asyncio.create_subprocess_shell(
        full_cmd,
        cwd=cwd,
        shell=True, executable="/bin/bash",
        stdout=subprocess.PIPE, stderr=subprocess.PIPE
    )
    out, err = await result.communicate()
    if out:
        LOGGER.info(out.decode())
    if err:
        LOGGER.error(err.decode())
    if output_file_path is not None:
        print(f"Writing output to {output_file_path}")
        with open(output_file_path, "w")  as output_file:
            output_file.write(out.decode())
    else:
        print(f'output_file_path: {output_file_path}')
    if result.returncode != 0 and not ignore_nonzero_returncode:
        raise ValueError(f"Error running command: '{command}'")

def get_aws_credentials(role_arn: str, session_name: str, with_credentials: Optional[Dict[str,str]]=None) -> Dict[str,str]:
    if with_credentials is None:
        sts_client = boto3.client("sts")
    else:
        sts_client = boto3.client(
            "sts",
            aws_access_key_id=with_credentials['AccessKeyId'],
            aws_secret_access_key=with_credentials['SecretAccessKey'],
            aws_session_token=with_credentials['SessionToken'],
        )
    creds = sts_client.assume_role(
        RoleArn=role_arn,
        RoleSessionName=session_name
    )["Credentials"]
    return {
        key: creds[key] for key in ["AccessKeyId", "SecretAccessKey", "SessionToken"]
    }
    

class Reporter:
    def __init__(self, websocket) -> None:
        self._websocket = websocket
        self._tasks = []

    def event(self, event: Dict[str, Any]) -> None:
        """
        quick events
        """
        self._tasks.append(
            asyncio.create_task(
                self._websocket.send(json.dumps({
                    "type": "event",
                    "data": event
                }))
            )
        )

    def build_status(self, event: Dict[str, Any]) -> None:
        """
        quick events
        """
        self._tasks.append(
            asyncio.create_task(
                self._websocket.send(json.dumps({
                    "type": "build_status",
                    "data": event
                }))
            )
        )

    def step_output(self, project_name: str, output: str) -> None:
        """
        quick events
        """
        self._tasks.append(
            asyncio.create_task(
                self._websocket.send(json.dumps({
                    "type": "build_output",
                    "data": {
                        "project_name": project_name,
                        "output": output
                    }
                }))
            )
        )

    async def close(self):
        for task in self._tasks:
            await task
